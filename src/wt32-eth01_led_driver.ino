// to enable DMA for the leds
#define FASTLED_ESP32_I2S true
#include "FastLED.h"

#include <ESParam.h>
#include <ArtNet.h>

// LED output pins
#define LED_DATA_1_PIN 2
#define LED_DATA_2_PIN 4
#define LED_DATA_3_PIN 12
#define LED_DATA_4_PIN 14
#define LED_DATA_5_PIN 15
#define LED_DATA_6_PIN 17
#define LED_DATA_7_PIN 5
#define LED_DATA_8_PIN 33
// other pins
#define STATUS_LED_PIN 32
#define INPUT_A_PIN    36
#define INPUT_B_PIN    39
// led config
#define LED_TYPE       WS2813
#define COLOR_ORDER    GRB
#define NUM_STRIPS     8
#define NUM_LEDS_PER_STRIP 300
#define NUM_LEDS NUM_LEDS_PER_STRIP * NUM_STRIPS
CRGB leds[NUM_LEDS];


// params
ColorParam colorParam;
IntParam brightnessParam;
BoolParam enableAnimParam;

void setup(){
    Serial.begin(115200);
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, HIGH);

    enableAnimParam.set("/anim/enable", true);
    enableAnimParam.saveType = SAVE_ON_REQUEST;

    brightnessParam.set("/leds/bright", 0, 255, 200);
    brightnessParam.setCallback(updateBrightness);
    brightnessParam.saveType = SAVE_ON_REQUEST;
    // uint32_t ha = CRGB(255,0,0);
    colorParam.set("/anim/color", 255);
    colorParam.saveType = SAVE_ON_REQUEST;

    paramCollector.add(&enableAnimParam);
    paramCollector.add(&colorParam);
    paramCollector.add(&brightnessParam);

    // artnetInit(&paramCollector);
    setupEsparam(&Serial);
    // wt32Params.wifiEnableParam.setBoolValue(0);
    startNetwork();
    artnetBegin();

    FastLED.addLeds<LED_TYPE, LED_DATA_1_PIN, COLOR_ORDER>(leds, 0, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_2_PIN, COLOR_ORDER>(leds, NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_3_PIN, COLOR_ORDER>(leds, 2 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_4_PIN, COLOR_ORDER>(leds, 3 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_5_PIN, COLOR_ORDER>(leds, 4 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_6_PIN, COLOR_ORDER>(leds, 5 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_7_PIN, COLOR_ORDER>(leds, 6 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    FastLED.addLeds<LED_TYPE, LED_DATA_8_PIN, COLOR_ORDER>(leds, 7 * NUM_LEDS_PER_STRIP, NUM_LEDS_PER_STRIP);//.setCorrection(TypicalLEDStrip);
    
    digitalWrite(STATUS_LED_PIN, LOW);
}

void loop(){
    digitalWrite(STATUS_LED_PIN, millis()%250 < 100);
    updateEsparam();
    // artnetUpdate(leds, NUM_LEDS);
    if(enableAnimParam.v){
        for(int i = 0; i < NUM_LEDS; i++){
            leds[i] = CHSV((i*3+millis()/10)%255, 255, 255);
        }
        // long t_stamp = micros();
        leds[0] = colorParam.v;
        FastLED.show();
        // Serial.println(micros()-t_stamp);
    }
}

void updateBrightness(int v){
    FastLED.setBrightness(v);
}